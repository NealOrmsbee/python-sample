"""Top-level WSGI interface script.

If the application is interfacing with a robust server like
Apache or NGINX via WSGI, this is the file to be designated
as the WSGIScript in a directive.

If run stand-alone, the application is served using werkzeug's
built-in run_simple method. Also a thread-profiling middleware
is applied. This is useful for locally testing performance issues.

"""

__author__ = 'neal'

import os
import site

from werkzeug.serving import run_simple
from werkzeug.contrib.profiler import ProfilerMiddleware

from app.config import config
from app.app import create

# Add this top-level directory as a site dir, to make importing all over the app easier.
pwd = os.path.dirname(os.path.abspath(__file__))
site.addsitedir(pwd)

# Top-level object named "application" is picked up by WSGI
application = create(config)

if __name__ == '__main__':
	# Serve app locally with thread profiler.
    application.wsgi_app = ProfilerMiddleware(application.wsgi_app, restrictions=[50])
    run_simple('localhost', 5001, application, use_reloader=True)