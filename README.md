# Python-Sample

## Abstract
The intent of this project is to demonstrate proficiency with Python and a number of popular frameworks including:

- Flask (Web-serving)
- SQLAlchmy (ORM)
- Nose (Tesing)

## Setup
As always, it is strongly recommended (see: insisted upon) that a [VirtualEnvironment](http://docs.python-guide.org/en/latest/dev/virtualenvs/)
be used for local testing and development.

After setting up and activating your environment, cd into the root of the project and run

	pip install -r requirements.txt

to install the third-party libraries.

You will also need to be able to connect to a MySQL database. The connection string can be set in the config.py file property SQLALCHEMY_DATABASE_URI
by editing the default string, or setting an environment variable. Whichever mysql daemon and DB are accessed, tables will be created as needed when
the application starts. This should be problem-free as long as it is a fresh DB on a recent version of MySQL.

## Running
Once setup is complete, simply cd into the project root and run command:

	python run.py

This will launch the application with a thread profiler in the terminal window.

The application will be running on

	127.0.0.1:5001