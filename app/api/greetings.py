__author__ = 'neal'

from flask import g, jsonify, request

from app import models


def greet(name):
	"""Return a greeting for `name`.

	Use a personalized greeting if one is available.

	"""

	person = g.session.query(models.Person).filter_by(name=name).first()
	if person is None:
		greeting = 'Hello'
	else:
		greeting = person.greeting.text or 'Hello'
	return greeting + ', ' + name + '!'


def set_greeting(name):
	"""Set a new, personalized greeting for `name`."""

	payload = g.payload
	greeting = payload.get('greeting') or None
	if greeting is None:
		raise HttpError(400, 'Request must include a JSON payload with a `greeting` parameter.')

	person = g.session.query(models.Person).filter_by(name=name).first() or models.Person(name=name, greeting=models.Greeting())
	
	person.greeting.text = greeting
	g.session.add(person)
	g.session.commit()
	return jsonify(person.serialize())