"""Hello resource routes."""

__author__ = 'neal'

from flask import Blueprint, request

from app.api import greetings

Hello = Blueprint('Hello', __name__)

@Hello.route('/hello', defaults={'name': 'world'}, methods=['GET', 'POST'])
@Hello.route('/hello/<name>', methods=['GET', 'POST'])
def greet(name):
	return {
		'GET': greetings.greet,
		'POST': greetings.set_greeting
	}[request.method](name)