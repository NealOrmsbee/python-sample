__author__ = 'neal'

"""Config options for the application environment.

The flask.Flask object has an attribute called config, which has a method called for_object(). That method, when given a
module with top-level variables in all-CAPS, it will configure itself to respect those variables. This is that config
module.

Notes
-----
We use os.environ because these options are largely dependent on the environment in which the app is being run. If no
config is supplied in the environment, all of the default settings assume local development.

"""

from os import environ as e

ACCESS_CONTROL_ALLOW_HEADERS = e.get('ACCESS_CONTROL_ALLOW_HEADERS', ['X-Requested-With', 'Content-Type', 'X-Authorization'])
ACCESS_CONTROL_ALLOW_ORIGINS = e.get('ACCESS_CONTROL_ALLOW_ORIGINS', ['*'])
ACCESS_CONTROL_ALLOW_METHODS = e.get('ACCESS_CONTROL_ALLOW_METHODS', ['GET', 'POST', 'PUT', 'PATCH', 'DELETE'])

ENVIRONMENT = e.get('ENVIRONMENT', 'local')
DEBUG = (ENVIRONMENT != 'production')

SQLALCHEMY_DATABASE_URI = e.get('SQLALCHEMY_DATABASE_URI', 'mysql://root@localhost/test?charset=utf8')