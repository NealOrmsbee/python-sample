__author__ = 'neal'

import flask

import models
from routes import Hello


def create(config):
	"""
	Initialize a flask.Flask application object.

	Given:
	    * a configuration module, `config`

	Return:
	    an initialized flask.Flask application object

	Parameters
	----------
	config : Python module

	Returns
	-------
	out : flask.Flask application

	"""

	# Instantiate Flask application object
	app = flask.Flask(__name__)

	# Apply module configuration
	app.config.from_object(config)

	# Establish pre- and post-request processing
	@app.before_request
	def before_request():
		"""Incoming-request preprocessing."""

		# Establish a database connection and attach to global "g" object.
		connection = models.SQLAlchemy(app.config['SQLALCHEMY_DATABASE_URI'])
		flask.g.session = connection.session

		# Put request payload into global object, if there is any.
		flask.g.payload = flask.request.get_json(silent=True) or {}

		# Normally this would be removed and we would use a tool like Alembic for establishing tables.
		connection.create_all()


	@app.after_request
	def set_headers(response):
		"""Response postprocessing."""

		# Add desired access control headers.
		response.headers.add(
			'Access-Control-Allow-Headers',
			', '.join(app.config['ACCESS_CONTROL_ALLOW_HEADERS'])
		)
		response.headers.add(
			'Access-Control-Allow-Methods',
			', '.join(app.config['ACCESS_CONTROL_ALLOW_METHODS'])
		)
		response.headers.add(
			'Access-Control-Allow-Origin',
			', '.join(app.config['ACCESS_CONTROL_ALLOW_ORIGINS'])
		)
		return response


	# Post-request teardown. Runs even on requests raising an exception.
	@app.teardown_request
	def teardown(e):
		"""Always close DB connection."""
		flask.g.session.close()


	# Bind routing Blueprints to application object
	app.register_blueprint(Hello)

	return app