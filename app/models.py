from sqlalchemy import Column, create_engine, ForeignKey, VARCHAR
from sqlalchemy.dialects.mysql import INTEGER as Integer
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship, backref

Base = declarative_base()


class SQLAlchemy(object):
    """Database session manager class."""

    engine = None
    session = None
    app = None

    def __repr__(self):
        return '<{0}.{1} object at {2}>'.format(
            self.__class__.__module__,
            self.__class__.__name__,
            hex(id(self)))

    def __init__(self, uri=None):
        if uri is not None:
            self.engine = create_engine(uri)
            Session = sessionmaker(bind=self.engine)
            self.session = Session()

    def init_app(self, app):
        self.engine = self.engine or create_engine(app.config['SQLALCHEMY_DATABASE_URI'])
        Session = sessionmaker(bind=self.engine)
        self.session = self.session or Session()
        self.app = app

    def create_all(self):
        """Create SQL tables.

        Issues CREATE TABLE statements for all tables defined by
        classes inheriting from `Base` to database connected to `engine`.

        """

        if not self.app or self.app.testing:
            Base.metadata.create_all(self.engine)

    def drop_all(self):
        """Drop SQL tables.

        Issues DROP TABLE statements for all tables defined by
        classes inheriting from `Base` to database connected to `engine.`
        USE WITH EXTREME CAUTION.
        """

        if not self.app or self.app.testing:
            Base.metadata.drop_all(self.engine)


class BaseModel(object):
    """A mixin class for establishing some nice defaults universal to our data models.

    This class is not meant to be used standalone,
    or for inheritance, but as a mixin with sqlalchemy's declarative base.

    """

    def __repr__(self):
        return '<{0} {1}>'.format(self.__class__.__name__, self.id_)




class Greeting(Base, BaseModel):
	"""Greeting.

	A simple SQLAlchemy model for storing a greeting string.

	"""

	__tablename__ = 'greetings'
	id_ = Column('id', Integer(unsigned=True), primary_key=True)
	text = Column(VARCHAR(255, collation='utf8_general_ci'))


class Person(Base, BaseModel):
    """Person.

    One who receives a greeting.

    """

    __tablename__ = 'people'
    id_ = Column('id', Integer(unsigned=True), primary_key=True)
    greeting_id = Column(Integer(unsigned=True), ForeignKey('greetings.id'), nullable=True)
    name = Column(VARCHAR(255, collation='utf8_general_ci'), unique=True)

    greeting = relationship('Greeting',
        backref='person', lazy='joined')

    def serialize(self):
        """Return a `dict` representation of the Person."""

        return {
            'id': self.id_,
            'name': self.name,
            'greeting': self.greeting.text
        }






